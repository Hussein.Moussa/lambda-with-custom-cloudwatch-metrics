import time
from random import randint

from aws_embedded_metrics import metric_scope
from aws_embedded_metrics.config import get_config

Config = get_config()
Config.namespace = "SampleLambdaWithCustomMetrics"


@metric_scope
def handle(event, context, metrics):
    for i in range(1000):
        print("Hello v2 at " + str(i))
        metrics.put_metric("Rest_API_Latency", randint(10, 5000), "Milliseconds")
        metrics.put_metric("TotalProcessingLatency", randint(10, 1000), "Milliseconds")
        time.sleep(1)
